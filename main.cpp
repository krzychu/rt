#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <gflags/gflags.h>

#include "accelerator.h"
#include "brdf.h"
#include "integrator.h"
#include "model.h"
#include "scene.h"
#include "stats.h"

using namespace rt;


DEFINE_string(scene, "", "Scene to render");
DEFINE_string(accelerator, "SimdBVH", "Accelerator to use");
DEFINE_string(integrator, "Path", "Integrator to use");
DEFINE_int32(bvh_leaf_threshold, 8, "Number of triangles in BVH leaf");
DEFINE_int32(samples_per_pixel, 32, "Number of samples per pixel");
DEFINE_string(diffuse, "Lambert", "diffuse BRDF");
DEFINE_string(specular, "Torrance", "specular BRDF");
DEFINE_int32(debug_pixel, -1, "pixel to debug");
DEFINE_int32(progress_frequency, 1000, "progress update frequency");


std::unique_ptr<BRDF> make_brdf(const std::string& name) {
  if (name == "Zero")
    return std::unique_ptr<BRDF>(new Zero);
  else if (name == "Phong")
    return std::unique_ptr<BRDF>(new Phong);
  else if (name == "Torrance")
    return std::unique_ptr<BRDF>(new Torrance);
  else if (name == "Lambert")
    return std::unique_ptr<BRDF>(new Lambert);
  else
    throw std::runtime_error("Unknown BRDF " + name);
}


int main(int argc, char** argv) {
  gflags::ParseCommandLineFlags(&argc, &argv, true);
  stats::init();

  std::cerr << "scene = " << FLAGS_scene << std::endl;
  std::cerr << "accelerator = " << FLAGS_accelerator << std::endl;
  std::cerr << "integrator = " << FLAGS_integrator << std::endl;
  std::cerr << "bvh_leaf_threshold = " << FLAGS_bvh_leaf_threshold << std::endl;
  std::cerr << "samples_per_pixel = " << FLAGS_samples_per_pixel << std::endl;
  std::cerr << "specular = " << FLAGS_specular << std::endl;
  std::cerr << "diffuse = " << FLAGS_diffuse << std::endl;
  std::cerr << "debug_pixel = " <<  FLAGS_debug_pixel << std::endl;
  
  dbg::set_debug_pixel(FLAGS_debug_pixel);

  auto scene = read_rgk_from_file(FLAGS_scene);
  std::cerr << "recursion_level = " << scene->recursion_level << std::endl;
  std::cerr << *scene << std::endl;
  
  stats::tic();
  using AccPtr = std::unique_ptr<Accelerator>;
  AccPtr accelerator;
  if (FLAGS_accelerator == "List")
    accelerator = AccPtr(new List<TriangleArray>(scene.get()));
  else if(FLAGS_accelerator == "SimdList")
    accelerator = AccPtr(new List<Triangle8Array>(scene.get()));
  else if(FLAGS_accelerator == "BVH")
    accelerator = AccPtr(
        new BVH<TriangleArray>(scene.get(), FLAGS_bvh_leaf_threshold));
  else if(FLAGS_accelerator == "SimdBVH")
    accelerator = AccPtr(
        new BVH<Triangle8Array>(scene.get(), FLAGS_bvh_leaf_threshold));
  else
    throw std::runtime_error("Unknown accelerator " + FLAGS_accelerator);
  stats::toc(std::cout, "Accelerator construction");


  auto specular = make_brdf(FLAGS_specular);
  auto diffuse = make_brdf(FLAGS_diffuse);

  using IntPtr = std::unique_ptr<Integrator>;
  IntPtr integrator;
  if (FLAGS_integrator == "Whitted")
    integrator = IntPtr(new WhittedIntegrator(
        scene.get(), 
        accelerator.get(), 
        specular.get(),
        diffuse.get(),
        scene->recursion_level));
  else if (FLAGS_integrator == "Direct") 
    integrator = IntPtr(new DirectIntegrator(
        scene.get(), 
        accelerator.get(), 
        specular.get(),
        diffuse.get()));
  else if (FLAGS_integrator == "Path") 
    integrator = IntPtr(new PathIntegrator(
        scene.get(), 
        accelerator.get(), 
        specular.get(),
        diffuse.get()));
  else
    throw std::runtime_error("Unknown integrator " + FLAGS_accelerator);

  auto rays = scene->camera->get_rays();
  std::vector<V> colors(scene->camera->resolution.get_num_pixels());
  const int size = rays.size();
  
  const float sample_weight = 1.0f / FLAGS_samples_per_pixel;
  stats::tic();

  int atomic_processed = 0;
  #pragma omp parallel for
  for (int i = 0; i < size; i++) {

    #pragma omp critical 
    {
      if (atomic_processed % FLAGS_progress_frequency == 0) {
        std::cout 
          << atomic_processed << "/"
          << rays.size() << " "
          << (100 * atomic_processed) / rays.size() << "% \n";
      }
      atomic_processed++;
    }

    dbg::set_current_pixel(i);
    V color(0);
    for (int j = 0; j < FLAGS_samples_per_pixel; j++) {
      color = color + sample_weight * integrator->integrate(rays[i], 0);
    }
    colors[i] = color;
  }
  stats::toc(std::cout, "Rendering");
  
  std::cerr << "output written to: " << scene->output_path << std::endl;
  scene->camera->save_exr(colors, scene->output_path);

  stats::print(std::cout);
  return 0;
}
