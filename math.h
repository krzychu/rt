// Realistyczna Grafika Komputerowa 2015/2016
// Krzysztof Chrobak
#pragma once

#include <algorithm>
#include <iostream>
#include <immintrin.h>
#include <mm_malloc.h>
#include <cmath>
#include <cassert>
#include <array>
  
#ifndef NDEBUG
#define ASSERT_UNIT( x ) assert(fabs(x.norm() - 1.f) < 1e-6);
#else
#define ASSERT_UNIT( x ) ;
#endif

namespace rt {

  inline int divup(int a, int b) {
    return (a + b - 1) / b;
  }

  inline float clamp(float x) {
    return std::min(1.0f, std::max(0.0f, x));
  }

  enum class Axis {
    X = 0, Y = 1, Z = 2
  };

  struct SimdFloat8 {
    __m256 v;

    SimdFloat8() {}

    explicit SimdFloat8(float x) {
      v = _mm256_set1_ps(x);
    }

    explicit SimdFloat8(const float* xs) {
      v = _mm256_load_ps(xs);    
    }

    SimdFloat8(__m256 v_) : v(v_) {}

    SimdFloat8 operator- () const {
      return SimdFloat8(0.) - *this;
    }
    
    SimdFloat8 operator+ (SimdFloat8 y) const {
      return SimdFloat8(_mm256_add_ps(v, y.v));
    }

    SimdFloat8 operator- (SimdFloat8 y) const {
      return SimdFloat8(_mm256_sub_ps(v, y.v));
    }

    SimdFloat8 operator* (SimdFloat8 y) const {
      return SimdFloat8(_mm256_mul_ps(v, y.v));
    }

    SimdFloat8 operator/ (SimdFloat8 y) const {
      return SimdFloat8(_mm256_div_ps(v, y.v));
    }

    void store(float * xs) const {
      _mm256_store_ps(xs, v);  
    }

    void load(const float * xs) {
      v = _mm256_load_ps(xs);
    }

    SimdFloat8 operator&& (const SimdFloat8& other) const {
      return _mm256_and_ps(v, other.v);
    }

    SimdFloat8 operator> (const SimdFloat8& other) const {
      return SimdFloat8(_mm256_cmp_ps(v, other.v, _CMP_GT_OS));
    }
  };


  std::ostream& operator<< (std::ostream& out, const SimdFloat8& x);


  template<class T>
  struct Traits {
    static T sqrt(const T& x);
    static T rsqrt(const T& x);
    static T reproc(const T& x);
    static constexpr size_t size();
    static constexpr T pos_inf();
    static constexpr T neg_inf();
  };

  template<>
  struct Traits<SimdFloat8> {
    static SimdFloat8 sqrt(const SimdFloat8& x) {
      return SimdFloat8(_mm256_sqrt_ps(x.v));
    }

    static SimdFloat8 rsqrt(const SimdFloat8& x) {
      return SimdFloat8(_mm256_rsqrt_ps(x.v));
    }

    static SimdFloat8 reproc(const SimdFloat8 x) {
      return SimdFloat8(_mm256_rcp_ps(x.v));
    }

    static constexpr size_t size() {
      return 8;
    }
  };


  template<>
  struct Traits<float> {
    static float sqrt(const float& x) {
      return ::sqrt(x);
    }

    static float rsqrt(const float& x) {
      return 1.0 / ::sqrt(x);
    }

    static constexpr size_t size() {
      return 1;
    }

    static constexpr float neg_inf() {
      return -1.0 / 0.0;
    }

    static constexpr float pos_inf() {
      return 1.0 / 0.0;
    }
  };


  template<class T>
  struct Vector3 {
    T x, y, z; 

    Vector3<T>() {}

    explicit Vector3<T>(float v) : x(v), y(v), z(v) {}

    explicit Vector3<T>(T x_, T y_, T z_) 
      : x(x_), y(y_), z(z_) {}

    Vector3<T> operator+ (const Vector3<T>& other) const {
      Vector3<T> result;
      result.x = x + other.x;
      result.y = y + other.y;
      result.z = z + other.z;
      return result;
    }

    Vector3<T> operator- (const Vector3<T>& other) const {
      Vector3<T> result;
      result.x = x - other.x;
      result.y = y - other.y;
      result.z = z - other.z;
      return result;
    }

    Vector3<T> operator* (T scale) const {
      Vector3<T> result;
      result.x = x * scale;
      result.y = y * scale;
      result.z = z * scale;
      return result;
    }

    Vector3<T> operator* (const Vector3<T>& other) const {
      Vector3<T> result;
      result.x = x * other.x;
      result.y = y * other.y;
      result.z = z * other.z;
      return result;
    }

    Vector3<T> operator/ (const Vector3<T>& other) const {
      Vector3<T> result;
      result.x = x / other.x;
      result.y = y / other.y;
      result.z = z / other.z;
      return result;
    }

    Vector3<T> cross(const Vector3<T>& other) const {
      Vector3<T> result;
      result.x = y * other.z - other.y * z;
      result.y = other.x * z - x * other.z;
      result.z = x * other.y - other.x * y;
      return result;
    }

    Vector3<T> normalized() const {
      T m = Traits<T>::rsqrt(x * x + y * y + z * z);
      Vector3<T> result;
      result.x = x * m;
      result.y = y * m;
      result.z = z * m;
      return result;
    }
    
    T norm() const {
      return Traits<T>::sqrt(x * x + y * y + z * z);
    }

    T dot(const Vector3<T>& other) const {
      return x * other.x + y * other.y + z * other.z;
    }

    T sum() const {
      return x + y + z;
    }

    bool is_zero() const {
      return x == 0 && y == 0 && z == 0; 
    }

    bool nonnegative() const {
      return x >= 0 && y >= 0 && z >= 0;
    }

    bool operator== (const Vector3<T> other) const {
      return (x == other.x) && (y == other.y) && (z == other.z); 
    }

    T operator[] (int i) const {
      return reinterpret_cast<const T*>(this)[i];
    }

    T& operator[] (int i) {
      return reinterpret_cast<T*>(this)[i];
    }
  };


  template<class T>
  Vector3<T> operator* (T scale, const Vector3<T>& v) {
    return v * scale;
  }

  
  template<class T>
  std::ostream& operator<< (std::ostream& out, const Vector3<T>& v) {
    out
      << "[" << v.x
      << " ," << v.y
      << " ," << v.z
      << "]";
    
    return out;
  }


  template<class T>
  std::istream& operator>> (std::istream& in, Vector3<T>& v) {
    in >> v.x >> v.y >> v.z; 
    return in;
  }

  using V = Vector3<float>;
  using SV8 = Vector3<SimdFloat8>;

  inline SV8 expand(const V& v) {
    SV8 result;
    result.x = SimdFloat8(v.x);
    result.y = SimdFloat8(v.y);
    result.z = SimdFloat8(v.z);
    return result;
  }

  struct V8 {
    const static size_t SIZE = 8;

    float x[SIZE];
    float y[SIZE];
    float z[SIZE];

    void set(int i, const V& v);
  };

  SV8 load(const V8& v);

  V8 store(const SV8& v);

  V min(const V& u, const V& v);

  V max(const V& u, const V& v);

  int argmax(const V& v);
  
  const float INVPI = 1.0f / M_PI;

  float randu();
}
