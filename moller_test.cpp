// Realistyczna Grafika Komputerowa 2015/2016
// Krzysztof Chrobak

#include <gtest/gtest.h>
#include "moller.h"
#include <iostream>

using namespace rt;

TEST(MollerTest, works) {
  Triangle triangle;
  triangle.a = V(1, 0, 0);
  triangle.b = V(0, 1, 0);
  triangle.c = V(0, 0, 1);
  triangle.tag = 1234;

  const V normal = 
    (triangle.b - triangle.a).cross(triangle.c - triangle.a).normalized();

  triangle.an = normal;
  triangle.bn = normal;
  triangle.cn = normal;
  
  Ray ray;
  ray.origin = V(0, 0, 0);
  ray.direction = V(1, 1, 1).normalized();

  Intersection intersection;
  moller::intersect(intersection, ray, triangle);
  
  const float x = sqrt(1.0 / 3.0);

  ASSERT_FLOAT_EQ(x, intersection.t);
  ASSERT_FLOAT_EQ(x, intersection.normal.x);
  ASSERT_FLOAT_EQ(x, intersection.normal.y);
  ASSERT_FLOAT_EQ(x, intersection.normal.z);
  ASSERT_FLOAT_EQ(1234, intersection.tag);
}
