// Realistyczna Grafika Komputerowa 2015/2016
// Krzysztof Chrobak

#include "camera.h"
#include <ImfRgbaFile.h>

namespace rt {

  std::ostream& operator << (std::ostream& out, const Resolution& resolution) {
    out 
      << "Resolution(width=" << resolution.width
      << ", height=" << resolution.height
      << ")";
    return out;
  }
  
  std::vector<Ray> Camera::get_rays() const { 
    std::vector<Ray> result;
    result.reserve(resolution.get_num_pixels());
  
    const V forward = (look_at - location).normalized();
    // const V forward = (location - look_at).normalized();
    const V upward = up.normalized();
    const V sideways = forward.cross(upward);

    float sensor_width_to_focal = 
      sensor_height_to_focal * resolution.get_width_to_height();

    for (int pixel_y = resolution.height - 1; pixel_y >= 0; pixel_y--) {
      const float shift_y = (pixel_y + 0.5) / resolution.height - 0.5;
      for (int pixel_x = 0; pixel_x < resolution.width; pixel_x++) {
        const float shift_x = (pixel_x + 0.5) / resolution.width - 0.5;

        const V direction = 
          forward + 
          shift_x * sensor_width_to_focal * sideways +
          shift_y * sensor_height_to_focal * upward;

        Ray ray;
        ray.origin = location;
        ray.direction = direction.normalized();

        result.push_back(ray);
      }
    }
  
    return result;
  }

  void Camera::save_exr(
      const std::vector<V>& colors, 
      const std::string& filename) const {
    Imf::Rgba* pixels = new Imf::Rgba[resolution.get_num_pixels()];
    int px = 0;
    for (const auto& color: colors) {
      pixels[px++] = Imf::Rgba(color.x, color.y, color.z, 0);
    }
    
    Imf::RgbaOutputFile file(
        filename.c_str(), 
        resolution.width, 
        resolution.height, 
        Imf::WRITE_RGBA);

    file.setFrameBuffer(pixels, 1, resolution.width);
    file.writePixels(resolution.height);

    delete[] pixels;
  }

  std::ostream& operator << (std::ostream& out, const Camera& camera) {
    out << "[[CAMERA]]" << std::endl;
    out << "resolution = " << camera.resolution << std::endl;
    out << "location = " << camera.location << std::endl;
    out << "look_at = " << camera.look_at << std::endl;
    out << "up = " << camera.up << std::endl;
    out << "sensor_height_to_focal = " << camera.sensor_height_to_focal << std::endl;
    return out;
  }
}
