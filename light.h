// Realistyczna Grafika Komputerowa 2015/2016
// Krzysztof Chrobak
#pragma once

#include "math.h"
#include "model.h"
#include "primitives.h"

namespace rt {

  struct LightSample {
    V radiance;
    Ray to_light;
    float probability;
    float distance;
  };

  
  struct Light {
    const static int NO_OBJECT_ID = -3;

    int object_id;
    Light(int object_id_=NO_OBJECT_ID) : object_id(object_id_) {}
    virtual LightSample sample(V point) const = 0;
    virtual ~Light() {}
  };


  struct PointLight : Light {
    V location;
    V color;
    float energy;

    virtual LightSample sample(V point) const;
  };


  struct MeshLight : Light {
    V emissive;
    std::vector<Triangle> triangles;
    std::vector<float> cdf;

    MeshLight(
        int object_id_, const V& emissive_, const std::vector<Triangle>& triangles_);

    virtual LightSample sample(V point) const;
  };


  std::ostream& operator << (std::ostream& out, const PointLight& light);
}
