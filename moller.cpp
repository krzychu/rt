// Realistyczna Grafika Komputerowa 2015/2016
// Krzysztof Chrobak

#include "moller.h"

namespace rt {
  namespace moller {

    void intersect(
        Intersection& result,
        const Ray& ray,
        const Triangle8& triangle) {

      const auto ta = load(triangle.a);
      const auto tb = load(triangle.b);
      const auto tc = load(triangle.c);

      const auto an = load(triangle.an);
      const auto bn = load(triangle.bn);
      const auto cn = load(triangle.cn);

      const auto p1 = tb - ta;
      const auto p2 = tc - ta;
      const auto orig = expand(ray.origin) - ta;
      const auto dir = expand(ray.direction);

      const auto p1_cross_p2 = p1.cross(p2);
      const auto det = -dir.dot(p1_cross_p2);

      //const auto inv_det = Traits<SimdFloat8>::reproc(det);
      const auto inv_det = SimdFloat8(1) / det;
      const auto dir_cross_orig = dir.cross(orig);
    
      const auto alpha = - inv_det * p2.dot(dir_cross_orig);
      const auto beta = inv_det * p1.dot(dir_cross_orig);
      auto t = inv_det * orig.dot(p1_cross_p2);
      
      const auto gamma = SimdFloat8(1.0) - alpha - beta;
      const auto normal = alpha * bn + beta * cn + gamma * an;
      
      float ts[8] __attribute__((aligned(32)));
      V8 norm = store(normal);
      
      t = (alpha > SimdFloat8(0.))
        && (beta > SimdFloat8(0.))
        && (gamma > SimdFloat8(0.))
        && (t > SimdFloat8(EPSILON))
        && t;

      t.store(ts);

      for (int i = 0; i < 8; i++) {
        const float x = ts[i];

        if (x > 0 && x < result.t && triangle.tag[i] != Intersection::NO_TAG) {
          result.t = x;
          result.tag = triangle.tag[i];
          result.normal.x = norm.x[i];
          result.normal.y = norm.y[i];
          result.normal.z = norm.z[i];
        }
      }
    }


    void intersect(
        Intersection& result,
        const Ray& ray, 
        const Triangle& triangle) {

      const auto p1 = triangle.b - triangle.a;
      const auto p2 = triangle.c - triangle.a;
      const auto orig = ray.origin - triangle.a;

      const auto p1_cross_p2 = p1.cross(p2);
      const auto det = - ray.direction.dot(p1_cross_p2);
      if (fabs(det) < 1e-5)
        return; 

      const auto inv_det = 1.0f / det;
      const auto dir_cross_orig = ray.direction.cross(orig);
    
      const auto alpha = - inv_det * p2.dot(dir_cross_orig);
      if (alpha < 0 || alpha > 1)
        return;

      const auto beta = inv_det * p1.dot(dir_cross_orig);
      if (beta < 0 || alpha + beta > 1)
        return;

      const auto t = inv_det * orig.dot(p1_cross_p2);
      if (t < EPSILON || t > result.t)
        return;
      
      result.t = t;
      result.normal = 
        alpha * triangle.bn 
        + beta * triangle.cn 
        + (1 - alpha - beta) * triangle.an;
      result.tag = triangle.tag;
    }
  } 
}
