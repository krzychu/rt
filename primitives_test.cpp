// Realistyczna Grafika Komputerowa 2015/2016
// Krzysztof Chrobak

#include <gtest/gtest.h>

#include "primitives.h"

using namespace rt;


TEST(TriangleTest, area) {
  Triangle t;
  t.b = V(0, 2, 0);
  t.c = V(0, 1, 1);
  ASSERT_FLOAT_EQ(1, t.area());
}


TEST(AABBTest, size_and_area) {
  AABB box;
  box.lower = V(1, 2, 3);
  box.upper = V(10, 20, 30);

  V size = box.size();
  ASSERT_EQ(V(9, 18, 27), size);
  
  ASSERT_FLOAT_EQ(
      9 * 18 + 9 * 27 + 18 * 27, box.area());
}


std::vector<AABB> make_boxes() {
  return {
    AABB( V(0, 0, 0), V(1, 2, 3) ),
    AABB( V(0, 0, 0), V(10, 1, 1) ),
    AABB( V(3, 4, 5), V(6, 7, 8) )
  };
}


TEST(AABBTest, bound_interval) {
  const auto boxes = make_boxes();
  auto bounds = bound(boxes.begin(), boxes.end());
  ASSERT_EQ(V(0, 0, 0), bounds.lower);
  ASSERT_EQ(V(10, 7, 8), bounds.upper);
}


TEST(AABBTest, bound_prefix) {
  const auto boxes = make_boxes();
  auto bounds = bound_prefix(boxes.begin(), boxes.end());
  ASSERT_EQ(4, bounds.size());
  
  ASSERT_EQ(V(Traits<float>::pos_inf()), bounds[0].lower);
  ASSERT_EQ(V(Traits<float>::neg_inf()), bounds[0].upper);
  ASSERT_FLOAT_EQ(Traits<float>::pos_inf(), bounds[0].area());
  
  ASSERT_EQ(V(0, 0, 0), bounds[1].lower);
  ASSERT_EQ(V(1, 2, 3), bounds[1].upper);

  ASSERT_EQ(V(0, 0, 0), bounds[2].lower);
  ASSERT_EQ(V(10, 2, 3), bounds[2].upper);

  ASSERT_EQ(V(0, 0, 0), bounds[3].lower);
  ASSERT_EQ(V(10, 7, 8), bounds[3].upper);
}


TEST(AABBTest, bound_suffix) {
  const auto boxes = make_boxes();
  auto bounds = bound_suffix(boxes.begin(), boxes.end());
  ASSERT_EQ(4, bounds.size());
  
  ASSERT_EQ(V(0, 0, 0), bounds[0].lower);
  ASSERT_EQ(V(10, 7, 8), bounds[0].upper);

  ASSERT_EQ(V(0, 0, 0), bounds[1].lower);
  ASSERT_EQ(V(10, 7, 8), bounds[1].upper);

  ASSERT_EQ(V(3, 4, 5), bounds[2].lower);
  ASSERT_EQ(V(6, 7, 8), bounds[2].upper);

  ASSERT_EQ(V(Traits<float>::pos_inf()), bounds[3].lower);
  ASSERT_EQ(V(Traits<float>::neg_inf()), bounds[3].upper);
}


TEST(AABBTest, intersection) {
  Ray ray;
  ray.origin = V(1, 1, 0);
  ray.direction = V(1, -1, 0).normalized();

  AABB box(V(2, -5, 0), V(7, 0, 0));

  auto intersection = box.intersect(ray);
  ASSERT_TRUE(intersection.does_intersect());
  ASSERT_FLOAT_EQ(sqrt(2), intersection.min_t);
  ASSERT_FLOAT_EQ(6 * sqrt(2), intersection.max_t);

  ray.origin = V(8, -6, 0);
  ray.direction = V(-1, 1, 0).normalized();

  intersection = box.intersect(ray);
  ASSERT_TRUE(intersection.does_intersect());
  ASSERT_FLOAT_EQ(sqrt(2), intersection.min_t);
  ASSERT_FLOAT_EQ(6 * sqrt(2), intersection.max_t);
}
