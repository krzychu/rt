// Realistyczna Grafika Komputerowa 2015/2016
// Krzysztof Chrobak
#pragma once

#include <string>
#include <iostream>
#include <utility>

namespace rt {
  namespace stats {
    void init();

    void add_ray(int level);

    void add_shadow_ray();

    void tic();
    
    void toc(std::ostream& out, const std::string& label);

    void print(std::ostream& out);
  }

  namespace dbg {
    void set_current_pixel(int num);

    void set_debug_pixel(int num); 

    bool is_debug_pixel();
  }
}
