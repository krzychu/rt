// Realistyczna Grafika Komputerowa 2015/2016
// Krzysztof Chrobak 

#pragma once

#include "math.h"
#include "model.h"

namespace rt {

  struct BRDFSample {
    float f;
    V direction;
    float probability;
  };


  struct BRDF {
    virtual BRDFSample sample(
        const Material& material, const V& normal, const V& in) const = 0;

    virtual float f(
        const Material& material, const V& normal, const V& in, const V& out) const = 0;

    virtual ~BRDF() {}
  };


  struct Phong : public BRDF {
    virtual BRDFSample sample(
        const Material& material, const V& normal, const V& in) const;

    virtual float f(
        const Material& material, const V& normal, const V& in, const V& out) const;
  };


  struct Lambert : public BRDF {
    virtual BRDFSample sample(
        const Material& material, const V& normal, const V& in) const;

    virtual float f(
        const Material& material, const V& normal, const V& in, const V& out) const;
  };


  struct Zero : public BRDF {
    virtual BRDFSample sample(
        const Material& material, const V& normal, const V& in) const;

    virtual float f(
        const Material& material, const V& normal, const V& in, const V& out) const;
  };


  struct Torrance : public BRDF {
    float blinn_density(const Material& material, float nh) const;

    virtual BRDFSample sample(
        const Material& material, const V& normal, const V& in) const;

    virtual float f(
        const Material& material, const V& normal, const V& in, const V& out) const;
  };
}
