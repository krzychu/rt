// Realistyczna Grafika Komputerowa 2015/2016
// Krzysztof Chrobak
#pragma once

#include <iostream>
#include <array>
#include "math.h"

namespace rt {
  
  struct Ray {
    V origin;
    V direction;

    V operator() (float t) const {
      return origin + t * direction;
    }
  }; 


  struct TriangleSample {
    V point;
    V normal;
  };

  
  struct Triangle {
    // vertices
    V a, b, c; 
   
    // normals
    V an, bn, cn;

    int tag;

    float area() const;

    TriangleSample sample() const;
  };


  struct AABBIntersection {
    float min_t, max_t;
    
    AABBIntersection()
      : min_t(Traits<float>::neg_inf()),
      max_t(Traits<float>::pos_inf()) {}

    AABBIntersection(float min_t_, float max_t_) 
      : min_t(min_t_), max_t(max_t_) {}

    bool does_intersect() const {
      return min_t < max_t && max_t > 0;
    }
  };


  struct AABB {
    V lower, upper;

    AABB() {}
    
    AABB(const V& lower_, const V& upper_) 
      : lower(lower_), upper(upper_) {} 

    V size() const {
      V v = upper - lower;
      v.x = fabs(v.x);
      v.y = fabs(v.y);
      v.z = fabs(v.z);
      return v;
    }

    float area() const {
      const auto s = size();
      return s.x * s.y + s.x * s.z + s.y * s.z;
    }

    AABBIntersection intersect(const Ray& ray) const;
  };

  
  std::ostream& operator << (std::ostream& out, const AABB& aabb);


  template<class T> 
  AABB bound(T begin, T end) {
    AABB result = *begin;
    while (begin != end) {
      result = bound(result, *begin);
      begin++;
    } 
    return result;
  }

  
  template<class T>
  std::vector<AABB> bound_prefix(T begin, T end) {
    const auto size = end - begin;
    std::vector<AABB> bounds(size + 1);
    bounds[0].lower = V(Traits<float>::pos_inf());
    bounds[0].upper = V(Traits<float>::neg_inf());
    for (int i = 0; i < size; i++) {
      bounds[i + 1] = bound(bounds[i], *begin);
      begin++;
    }
    return bounds;
  }

  
  template<class T> 
  std::vector<AABB> bound_suffix(T begin, T end) {
    const auto size = end - begin;
    std::vector<AABB> bounds(size + 1);
    bounds[size].lower = V(Traits<float>::pos_inf());
    bounds[size].upper = V(Traits<float>::neg_inf());
    for (int i = size - 1; i >= 0; i--) {
      end--;
      bounds[i] = bound(bounds[i + 1], *end);
    }
    return bounds;
  }


  AABB bound(const AABB& x, const AABB& y);


  struct Triangle8 {
    using TriItr = std::vector<Triangle>::const_iterator;

    V8 a, b, c;
    V8 an, bn, cn;
    uint32_t tag[V8::SIZE];

    Triangle8(TriItr begin, TriItr end);
    Triangle8();
  };


  struct Intersection {
    const static uint32_t NO_TAG;
    float t;
    V normal;
    uint32_t tag;

    Intersection() : t(1.0 / 0.0), normal(0), tag(NO_TAG) {}

    bool does_intersect() const {
      return tag != NO_TAG;
    }
  };

  
  std::ostream& operator<< (
      std::ostream& out, const Intersection& intersection);


  struct Triangle8Array {
    explicit Triangle8Array(int size_ = 0);

    ~Triangle8Array();
    
    Triangle8Array(const Triangle8Array& other);

    const Triangle8Array& operator= (const Triangle8Array& other);

    Triangle8& operator[] (int index) { return triangles[index]; } 

    const Triangle8& operator[] (int index) const { return triangles[index]; } 

    size_t size() const { return num_triangles; }

    const Triangle8* cbegin() const { return triangles; };

    const Triangle8* cend() const { return triangles + num_triangles; };

    Triangle8* begin() { return triangles; };

    Triangle8* end() { return triangles + num_triangles; };

    Triangle8* triangles;
    size_t num_triangles;
  };

  
  using TriangleArray = std::vector<Triangle>;

  template<class Storage> 
  struct StorageTraits {
    static int num_triangles_in_single_element();
  };

  template<> 
  struct StorageTraits<Triangle8Array> {
    static int num_triangles_in_single_element() { return 8; } 
  };

  template<> 
  struct StorageTraits<TriangleArray> {
    static int num_triangles_in_single_element() { return 1; } 
  };

}
