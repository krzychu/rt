// Realistyczna Grafika Komputerowa 2015/2016
// Krzysztof Chrobak
#pragma once

#include "primitives.h"
#include "scene.h"
#include "moller.h"


namespace rt {

  struct Accelerator {
    virtual ~Accelerator() {}
    
    virtual Intersection intersect(const Ray& ray) const = 0;
    virtual bool does_intersect(const Ray& ray, float max_distance) const = 0;
  };


  template<class Itr>
  Triangle8* aggregate_triangles(Itr begin, Itr end, Triangle8* out) {
    const size_t size = end - begin;
    for (size_t i = 0; i < size; i += V8::SIZE) {
      const size_t j = std::min(i + V8::SIZE, size);
      
      *(out++) = Triangle8(begin + i, begin + j);
    }
    return out;
  }

  
  template<class Itr>
  TriangleArray::iterator aggregate_triangles(
      Itr begin, Itr end, TriangleArray::iterator out) {
    std::copy(begin, end, out); 
    return out + (end - begin);
  }


  template<class Itr>
  void intersect_range(
      Intersection& result,
      const Ray& ray,
      Itr begin, 
      Itr end) {

    while (begin != end) {
      moller::intersect(result, ray, *begin);
      begin++;
    }
  }


  template<class Itr>
  bool does_intersect_range(
      const Ray& ray,
      Itr begin, 
      Itr end,
      float max_distance) {

    while (begin != end) {
      if (moller::does_intersect(ray, *begin, max_distance)) {
        return true;
      }
      begin++;
    }
    return false;
  }

  
  template<class Storage>
  struct List : public Accelerator {
    List(const Scene* scene);

    Intersection intersect(const Ray& ray) const override;

    bool does_intersect(const Ray& ray, float max_distance) const override;

    Storage triangles;
  };

  
  template<class Storage>
  List<Storage>::List(const Scene* scene) {
    auto single_triangles = get_triangles(*scene);
    const size_t size = divup(
        single_triangles.size(), 
        StorageTraits<Storage>::num_triangles_in_single_element());

    triangles = Storage(size);

    aggregate_triangles(
        single_triangles.begin(), single_triangles.end(), triangles.begin());
  }

    
  template<class Storage>
  Intersection List<Storage>::intersect(const Ray& ray) const {
    Intersection result;
    intersect_range(result, ray, triangles.cbegin(), triangles.cend());
    return result;
  }


  template<class Storage>
  bool List<Storage>::does_intersect(const Ray& ray, float max_distance) const {
    return does_intersect_range(
        ray, triangles.cbegin(), triangles.cend(), max_distance);
  }
  

  struct TriAABB : public AABB {
    V center;
    int triangle_id;

    TriAABB() {};
  
    TriAABB(const Triangle& triangle, int triangle_id_);
  };
  

  enum class BVHNodeType {
    Leaf, Split
  };


  struct BVHNode {
    BVHNodeType type;
    AABB bounds;
    int begin, end;

    BVHNode() {}

    BVHNode(BVHNodeType type_, const AABB& bounds_, int begin_, int end_) 
      : type(type_), bounds(bounds_), begin(begin_), end(end_) {}

    int num_triangles() const { return end - begin; }

    bool is_leaf() const { return type == BVHNodeType::Leaf; }
  };


  std::ostream& operator << (std::ostream& out, const BVHNode& node);


  struct PreBVH {
    PreBVH(const Scene* scene, size_t leaf_threshold_);
         
    int build(
        int begin, int end,
        std::vector<TriAABB>& boxes);

    std::vector<BVHNode> nodes;
    std::vector<Triangle> triangles;
    int leaf_threshold;
    int root;
  };

  
  template<class Storage>
  struct BVH : public Accelerator {
    BVH(const Scene* scene, size_t leaf_threshold_ = 1);

    Intersection intersect(const Ray& ray) const override;

    void intersect_node(
        Intersection& result, int node_id, const Ray& ray) const;

    bool does_intersect(const Ray& ray, float max_distance) const override;

    bool does_intersect_node(
        int node_id, const Ray& ray, float max_distance) const;

    void debug_print(std::ostream& out, int node, int indent) const;
    
    std::vector<BVHNode> nodes;
    Storage triangles;
    int leaf_threshold;
    int root;
  };

  
  template<class Storage>
  BVH<Storage>::BVH(const Scene* scene, size_t leaf_threshold_) {
    const int in_element = 
      StorageTraits<Storage>::num_triangles_in_single_element();

    auto pre = PreBVH(scene, leaf_threshold_ * in_element);
    // const auto size = divup(pre.triangles.size(), in_element);
    const auto size = pre.triangles.size();

    leaf_threshold = pre.leaf_threshold;
    nodes = pre.nodes;
    root = pre.root;
    triangles = Storage(size);
    
    auto pre_begin = pre.triangles.begin();
    auto begin = triangles.begin();
    for (auto& node : nodes) {
      if (!node.is_leaf())
        continue;
    
      const auto end = aggregate_triangles(
          pre_begin + node.begin,
          pre_begin + node.end,
          begin);

      node.begin = begin - triangles.begin();
      node.end = end - triangles.begin();
      begin = end;
    }
  }


  template<class Storage>
  Intersection BVH<Storage>::intersect(const Ray& ray) const {
    Intersection result;
    const BVHNode& root_node = nodes[root];
    const auto root_intersection = root_node.bounds.intersect(ray);
    if (root_intersection.does_intersect()) {
      intersect_node(result, root, ray);
    }
    return result;
  }


  template<class Storage>
  void BVH<Storage>::intersect_node(
      Intersection& result, int node_id, const Ray& ray) const {

    const BVHNode& current = nodes[node_id];

    if (current.is_leaf()) {
      intersect_range(
          result, ray, 
          triangles.cbegin() + current.begin, 
          triangles.cbegin() + current.end);

    } else {
      auto first = current.begin;
      auto second = current.end;

      const BVHNode& left = nodes[first];
      const BVHNode& right = nodes[second];

      auto first_intersection = left.bounds.intersect(ray);
      auto second_intersection = right.bounds.intersect(ray);

      if (first_intersection.min_t > second_intersection.min_t) {
        std::swap(first, second);
        std::swap(first_intersection, second_intersection);
      }
      
      if (first_intersection.does_intersect())
        intersect_node(result, first, ray);
      
      if (second_intersection.does_intersect() 
          && (!result.does_intersect() || result.t > second_intersection.min_t))
        intersect_node(result, second, ray);
    }
  }

  template<class Storage>
  bool BVH<Storage>::does_intersect(const Ray& ray, float max_distance) const {
    const BVHNode& root_node = nodes[root];
    const auto root_intersection = root_node.bounds.intersect(ray);
    return 
      root_intersection.does_intersect() 
      && does_intersect_node(root, ray, max_distance);
  }


  template<class Storage>
  bool BVH<Storage>::does_intersect_node(
      int node_id, const Ray& ray, float max_distance) const {

    const BVHNode& current = nodes[node_id];

    if (current.is_leaf()) {
      return does_intersect_range(
          ray, 
          triangles.cbegin() + current.begin, 
          triangles.cbegin() + current.end,
          max_distance);

    } else {
      auto first = current.begin;
      auto second = current.end;

      const BVHNode& left = nodes[first];
      const BVHNode& right = nodes[second];

      auto first_intersection = left.bounds.intersect(ray);
      auto second_intersection = right.bounds.intersect(ray);

      if (first_intersection.min_t > second_intersection.min_t) {
        std::swap(first, second);
        std::swap(first_intersection, second_intersection);
      }
      
      bool isect = false;
      if (first_intersection.does_intersect() 
          && first_intersection.min_t <= max_distance)
        isect = does_intersect_node(first, ray, max_distance);
      
      if (!isect 
          && second_intersection.does_intersect() 
          && second_intersection.min_t <= max_distance)
        isect = does_intersect_node(second, ray, max_distance);

      return isect;
    }
  }

  
  template<class Storage>
  void BVH<Storage>::debug_print(std::ostream& out, int node, int indent) const {
    const BVHNode& current = nodes[node];
  
    for (int i = 0; i < indent; i++)
      out << " "; 

    if (current.is_leaf())
      out << "Leaf ";
    else
      out << "Split ";

    out << current << std::endl;

    if (!current.is_leaf()) {
      debug_print(out, current.begin, indent + 1);
      debug_print(out, current.end, indent + 1);
    }
  }
  
  template<class Storage>
  std::ostream& operator << (std::ostream& out, const BVH<Storage>& bvh) {
    bvh.debug_print(out, bvh.root, 0);
    return out;
  }
}
