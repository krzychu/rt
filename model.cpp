// Realistyczna Grafika Komputerowa 2015/2016
// Krzysztof Chrobak

#include "model.h"
#include <string>
#include <sstream>
#include <cctype>
#include <cstdio>
#include <fstream>


namespace rt {

  bool is_white(char x) {
    return isspace(x) || x == '\r';
  }

  std::string strip(const std::string str) {
    auto begin = str.begin();

    while (begin != str.end() && is_white(*begin))
      begin++;

    auto end = begin;
    while (end != str.end() && !is_white(*end))
      end++;
    
    return std::string(begin, end);
  }
  
  std::string get_other_file(
      const std::string& other_file, 
      const std::string& base_path) {

    auto begin = base_path.rbegin();
    auto end = base_path.rend();
    while (begin != end && *begin != '/') {
      begin++; 
    }

    auto prefix = std::string(begin, end);
    std::reverse(prefix.begin(), prefix.end());
    
    return strip(prefix + other_file);
  }

  std::ostream& operator << (std::ostream& out, const FaceNode& node) {
    out << "[v: " << node.v << " t: " << node.t << " n: " << node.n << "]";
    return out;
  }

  std::ostream& operator << (std::ostream& out, const Object& object) {
    out << "[[OBJECT]]" << std::endl;
    out << "material_name = " << object.material_name << std::endl;
    out << "num_faces = " << object.faces.size() << std::endl;
    return out;
  }

  std::ostream& operator << (std::ostream& out, const Material& material) {
    out << "[[MATERIAL]]" << std::endl;
    out << "ambient = " << material.ambient << std::endl;
    out << "diffuse = " << material.diffuse << std::endl;
    out << "specular = " << material.specular << std::endl;
    out << "specular_exponent = " << material.specular_exponent << std::endl;
    out << "reflectivity = " << material.reflectivity << std::endl;
    return out;
  }

  MaterialMap read_mtl_from_file(
      const std::string& mtl_path) {

    std::fstream fs(mtl_path);
    if (!fs) {
      throw std::runtime_error("Unable to open: " + mtl_path);
    }

    return read_mtl(fs);
  }

  MaterialMap read_mtl(std::istream& in) {
    MaterialMap result;
    std::string line;
    std::string current_name = "";
    while (!in.eof()) {
      std::getline(in, line); 
      std::stringstream line_stream(line);

      std::string command;
      line_stream >> command;

      if (command == "newmtl") {
        line_stream >> current_name;
      } else if (current_name == "") {
        continue;
      }

      Material& mtl = result[current_name];
        
      if (command == "reflectivity") {
        line_stream >> mtl.reflectivity;
      } else if (command == "Ns") {
        line_stream >> mtl.specular_exponent;
      } else if (command == "Ni") {
        line_stream >> mtl.refraction_index;
      } else if (command == "Ka") {
        line_stream >> mtl.ambient;
      } else if (command == "Kd") {
        line_stream >> mtl.diffuse;
      } else if (command == "Ks") {
        line_stream >> mtl.specular;
      } else if (command == "Ke") {
        line_stream >> mtl.emissive;
      }
    }
    return result;
  }

  std::unique_ptr<Model> read_obj(std::istream& in, const std::string& obj_path) {
    std::unique_ptr<Model> model(new Model());
    
    std::unique_ptr<Object> current(new Object("Default"));
    std::string line;
    int line_number = 0;
    while (!in.eof()) {
      std::getline(in, line);
      line_number++;
      std::stringstream line_stream(line);
    
      std::string command;
      line_stream >> command;
      
      if (command == "v") {
        V vertex;
        line_stream >> vertex;
        model->vertices.push_back(vertex);
      } else if (command == "vn") {
        V normal;
        line_stream >> normal;
        model->normals.push_back(normal);
      } else if (command == "f") {
        std::vector<FaceNode> face;
        while (true) {
          std::string part;
          line_stream >> part;
          if (!part.size()) {
            break;
          }
          
          int v, t, n;
          FaceNode node;
          if(sscanf(part.c_str(), "%d/%d/%d", &v, &t, &n) == 3) {
            node.v = v - 1;
            node.t = t - 1;
            node.n = n - 1;
          } else if(sscanf(part.c_str(), "%d//%d", &v, &n) == 2) {
            node.v = v - 1;
            node.n = n - 1;
          } else if(sscanf(part.c_str(), "%d/%d", &v, &t) == 2) {
            node.v = v - 1;
            node.t = t - 1;
          } else if(sscanf(part.c_str(), "%d", &v) == 1) {
            node.v = v - 1;
          }

          face.push_back(node);
        }
        assert(current != nullptr);
        current->faces.push_back(face);
      } else if (command == "mtllib") {
        std::string mtl_path;
        line_stream >> mtl_path;
        mtl_path = get_other_file(mtl_path, obj_path);
        model->materials = read_mtl_from_file(mtl_path);
      } else if (command == "o") {
        if (current->faces.size()) {
          model->objects.push_back(std::move(current));
        }
        std::string name;
        line_stream >> name;
        current = std::unique_ptr<Object>(new Object(name));
      } else if (command == "usemtl") {
        std::string material_name;
        line_stream >> material_name;

        if (current->faces.size()) {
          model->objects.push_back(std::move(current));
        }
        std::stringstream ss;
        ss << material_name << "_" << line_number;
        current = std::unique_ptr<Object>(new Object(ss.str()));
        current->material_name = material_name;
      }
    }
    if (current->faces.size()) {
      model->objects.push_back(std::move(current));
    }

    return model;
  }

  std::ostream& operator << (std::ostream& out, const Model& model) {
    out << "[[MODEL]]" << std::endl;
    out << "num_vertices = " << model.vertices.size() << std::endl;
    out << "num_normals = " << model.normals.size() << std::endl;

    out << "materials" << std::endl; 
    for (const auto& kvp : model.materials) {
      out << kvp.second;
      out << "name = " << kvp.first << std::endl;
    }

    out << "objects:" << std::endl;
    for (const auto& object : model.objects) {
      out << *object;
    }
    return out;
  }

  std::unique_ptr<Model> read_obj_from_file(
      const std::string& obj_path) {

    std::ifstream fs(obj_path);
    if (!fs) {
      throw std::runtime_error("Unable to open: " + obj_path);
    }

    return read_obj(fs, obj_path);
  }

  std::vector<Triangle> get_triangles(
      int id, const Model& model, const Object& object) {

    std::vector<Triangle> triangles;
    for (const auto& face : object.faces) {
      if (face.size() != 3) {
        throw std::runtime_error("Face with more than three vertices");
      }

      Triangle t;
      t.a = model.vertices[face[0].v];
      t.b = model.vertices[face[1].v];
      t.c = model.vertices[face[2].v];

      const auto normal = (t.b - t.a).cross(t.c - t.a).normalized();
      t.an = (face[0].n != MISSING) ? model.normals[face[0].n] : normal;
      t.bn = (face[1].n != MISSING) ? model.normals[face[1].n] : normal;
      t.cn = (face[2].n != MISSING) ? model.normals[face[2].n] : normal;

      t.tag = id;
      triangles.push_back(t);
    } 

    return triangles;
  }
}
