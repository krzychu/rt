// Realistyczna Grafika Komputerowa 2015/2016
// Krzysztof Chrobak

#include <gtest/gtest.h>

#include "math.h"

using namespace rt;

TEST(SimdFloat8Test, can_be_initialized) {
  SimdFloat8 x(1.23);
  float xs[8];
  x.store(xs);
  
  for (int i = 0; i < 8; i++) {
    ASSERT_FLOAT_EQ(1.23, xs[i]);
  }
}


TEST(SimdFloat8Test, addition_works) {
  float xs[8] = {1, 2, 3, 4, 5, 6, 7, 8};
  float ys[8] = {3, 4, 5, 6, 7, 8, 9, 10};
  float zs[8];

  SimdFloat8 x(xs), y(ys);
  SimdFloat8 z = x + y;

  z.store(zs);
  for (int i = 0; i < 8; i++) {
    ASSERT_FLOAT_EQ(2 * i + 4, zs[i]);
  }
}


TEST(SimdFloat8Test, subtraction_works) {
  float xs[8] = {1, 2, 3, 4, 5, 6, 7, 8};
  float ys[8] = {3, 4, 5, 6, 7, 8, 9, 10};
  float zs[8];

  SimdFloat8 x(xs), y(ys);
  SimdFloat8 z = y - x;

  z.store(zs);
  for (int i = 0; i < 8; i++) {
    ASSERT_FLOAT_EQ(2, zs[i]);
  }
}


TEST(SimdFloat8Test, multiplication_works) {
  float xs[8] = {1, 2, 3, 4, 5, 6, 7, 8};
  float ys[8] = {3, 4, 5, 6, 7, 8, 9, 10};
  float zs[8];

  SimdFloat8 x(xs), y(ys);
  SimdFloat8 z = x * y;

  z.store(zs);
  for (int i = 0; i < 8; i++) {
    ASSERT_FLOAT_EQ((i + 1) * (i + 3), zs[i]);
  }
}


TEST(SimdFloat8Test, division_works) {
  float xs[8] = {1, 2, 3, 4, 5, 6, 7, 8};
  float ys[8] = {3, 4, 5, 6, 7, 8, 9, 10};
  float zs[8];

  SimdFloat8 x(xs), y(ys);
  SimdFloat8 z = x / y;

  z.store(zs);
  for (int i = 0; i < 8; i++) {
    ASSERT_FLOAT_EQ((i + 1) / (float)(i + 3), zs[i]);
  }
}


TEST(SimdFloat8Test, comparison_works) {
  float xs[8] = {12.1173, 2, 3, 4, 5, 6, 7, 8};
  float ys[8] = {12.1183, 4, 5, 6, 7, 8, 9, 10};

  SimdFloat8 x(xs), y(ys);
  SimdFloat8 z = x > y;

  float ts[8] __attribute__((aligned(32)));
  z.store(ts);

  for (int i = 0; i < 8; i++) {
    ASSERT_EQ(0, ts[i]);
  }
}


TEST(Vector3Test, initialization_works) {
  Vector3<float> v(1.23);
  ASSERT_FLOAT_EQ(v.x, 1.23);
  ASSERT_FLOAT_EQ(v.y, 1.23);
  ASSERT_FLOAT_EQ(v.z, 1.23);
}


TEST(Vector3Test, addition_works) {
  Vector3<float> x;
  x.x = 1;
  x.y = 2;
  x.z = 3;
  
  Vector3<float> y;
  y.x = 4;
  y.y = 5;
  y.z = 6;

  auto z = x + y;
  ASSERT_FLOAT_EQ(5, z.x);
  ASSERT_FLOAT_EQ(7, z.y);
  ASSERT_FLOAT_EQ(9, z.z);
}


TEST(Vector3Test, cross_works) {
  Vector3<float> u;
  u.x = 1;
  u.y = 2;
  u.z = 3;
  
  Vector3<float> v;
  v.x = 4;
  v.y = 5;
  v.z = 6;

  auto t = u.cross(v);
  ASSERT_FLOAT_EQ(-3, t.x);
  ASSERT_FLOAT_EQ(6, t.y);
  ASSERT_FLOAT_EQ(-3, t.z);
}


TEST(Vector3Test, dot_works) {
  Vector3<float> u;
  u.x = 1;
  u.y = 2;
  u.z = 3;
  
  Vector3<float> v;
  v.x = 4;
  v.y = 5;
  v.z = 6;

  auto t = u.dot(v);
  ASSERT_FLOAT_EQ(4 + 10 + 18, t);
}


TEST(Vector3Test, element_access) {
  V v(1, 2, 3);
  ASSERT_EQ(1, v[0]);
  ASSERT_EQ(2, v[1]);
  ASSERT_EQ(3, v[2]);
}
