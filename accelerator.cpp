// Realistyczna Grafika Komputerowa 2015/2016
// Krzysztof Chrobak

#include "accelerator.h"
#include "moller.h"


namespace rt {
  std::vector<Triangle> get_triangles(const Scene* scene) {
    std::vector<Triangle> triangles;
    int object_id = 0;
    for (const auto& object : scene->model->objects) {
      for (const auto& face : object->faces) {
        if (face.size() != 3) {
          throw std::runtime_error("Face with more than three vertices");
        }
        Triangle t;
        t.a = scene->model->vertices[face[0].v];
        t.b = scene->model->vertices[face[1].v];
        t.c = scene->model->vertices[face[2].v];
      
        const auto normal = (t.b - t.a).cross(t.c - t.a).normalized();
        t.an = (face[0].n != MISSING) ? scene->model->normals[face[0].n] : normal;
        t.bn = (face[1].n != MISSING) ? scene->model->normals[face[1].n] : normal;
        t.cn = (face[2].n != MISSING) ? scene->model->normals[face[2].n] : normal;

        t.tag = object_id;
        triangles.push_back(t);
      } 
      object_id++;
    }
    return triangles;
  }


  TriAABB::TriAABB(const Triangle& triangle_, int triangle_id_) {
    lower = min(triangle_.a, min(triangle_.b, triangle_.c)),
    upper = max(triangle_.a, max(triangle_.b, triangle_.c)),
    center = (1.0f / 3.0f) * (triangle_.a + triangle_.b + triangle_.c);
    triangle_id = triangle_id_;
  }


  std::ostream& operator << (std::ostream& out, const BVHNode& node) {
    out 
      << "BVHNode(begin=" << node.begin 
      << ", end=" << node.end
      << ", bounds=" << node.bounds
      << ")";

    return out;
  }


  PreBVH::PreBVH(const Scene* scene, size_t leaf_threshold_) {
    const auto tmp_triangles = get_triangles(scene);
    nodes.reserve(tmp_triangles.size());
    leaf_threshold = leaf_threshold_;
    
    std::vector<TriAABB> boxes(tmp_triangles.size());
    for (size_t i = 0; i < tmp_triangles.size(); i++) {
      boxes[i] = TriAABB(tmp_triangles[i], i);
    }
    
    root = build(0, boxes.size(), boxes);

    triangles.resize(tmp_triangles.size());
    for (size_t i = 0; i < boxes.size(); i++) {
      triangles[i] = tmp_triangles[boxes[i].triangle_id];
    }
  }


  int PreBVH::build(
      int begin, int end,
      std::vector<TriAABB>& boxes) {

    auto begin_itr = boxes.begin() + begin;
    auto end_itr = boxes.begin() + end;
    
    auto node_bounds = bound(begin_itr, end_itr);
    for (int i = 0; i < 3; i++) {
      float& u = node_bounds.upper[i];
      float& l = node_bounds.lower[i];
      if (u - l < 1e-3) {
        u += 1e-3;
        l -= 1e-3;
      }
    }
    const auto node_size = node_bounds.size();
    const int num_boxes = end - begin;

    if (num_boxes <= leaf_threshold) {
      const int idx = nodes.size();
      nodes.push_back(BVHNode(BVHNodeType::Leaf, node_bounds, begin, end));
      return idx;
    }

    const int longest_dimension = argmax(node_size);

    auto cmp = [=] (const TriAABB& a, const TriAABB& b) {
      return a.center[longest_dimension] > b.center[longest_dimension];
    };

    std::sort(begin_itr, end_itr, cmp);
    
    const auto prefix = bound_prefix(begin_itr, end_itr);  
    const auto suffix = bound_suffix(begin_itr, end_itr);
    
    const float total_area = node_bounds.area();
    float best_split = 0;
    float min_sah = Traits<float>::pos_inf();
    for (int i = 1; i < num_boxes; i++) {
      const float left_area = prefix[i].area();
      const float right_area = suffix[i].area();

      const float sah = 
        1.0 / total_area * (left_area * i + right_area * (num_boxes - i));

      if (sah < min_sah) {
        min_sah = sah;
        best_split = i;
      }
    } 
    
    nodes.push_back(
        BVHNode(
          BVHNodeType::Split,
          node_bounds, 
          build(begin, begin + best_split, boxes),
          build(begin + best_split, end, boxes)));
    return nodes.size() - 1;
  }

}
