// Realistyczna Grafika Komputerowa 2015/2016
// Krzysztof Chrobak
#pragma once

#include "accelerator.h"
#include "math.h"
#include "primitives.h"
#include "scene.h"
#include "brdf.h"

#include <memory>

namespace rt {

  class Integrator {
    public:
      Integrator(
          const Scene* scene_, 
          const Accelerator* accelerator_,
          const BRDF* specular_,
          const BRDF* diffuse_) 
        : scene(scene_), 
        accelerator(accelerator_), 
        specular(specular_),
        diffuse(diffuse_) {}

      virtual ~Integrator() {}
      virtual V integrate(const Ray& ray, int level) const = 0;
    
    protected:
      const Scene* scene;
      const Accelerator* accelerator;
      const BRDF* specular;
      const BRDF* diffuse;

      bool intersect(
          const Ray& ray,
          Intersection* intersection,
          Material* material,
          V* point) const;

      V get_light_contribution(
          const Light& light, 
          const Ray& ray,
          V point, 
          V normal,
          const Material& material) const;
      
      V get_all_lights_contributions(
          const Ray& ray,
          V point, 
          V normal,
          const Material& material) const;
  };


  class WhittedIntegrator : public Integrator {
    public:
      WhittedIntegrator(
          const Scene* scene_, 
          const Accelerator* accelerator_,
          const BRDF* specular_,
          const BRDF* diffuse_,
          int max_level_)
        : Integrator(scene_, accelerator_, specular_, diffuse_), 
        max_level(max_level_) {}

      V integrate(const Ray& ray, int level) const override;

    private:
      int max_level;
  };
 

  class DirectIntegrator : public Integrator {
    public:
      DirectIntegrator(
          const Scene* scene_, 
          const Accelerator* accelerator_,
          const BRDF* specular_,
          const BRDF* diffuse_)
        : Integrator(scene_, accelerator_, specular_, diffuse_) {}

      V integrate(const Ray& ray, int level) const override;
  };


  class PathIntegrator : public Integrator {
    public:
      PathIntegrator(
          const Scene* scene_, 
          const Accelerator* accelerator_,
          const BRDF* specular_,
          const BRDF* diffuse_)
        : Integrator(scene_, accelerator_, specular_, diffuse_) {}

      V integrate(const Ray& ray, int level) const override;
  };
}
