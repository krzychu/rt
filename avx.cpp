// Realistyczna Grafika Komputerowa 2015/2016
// Krzysztof Chrobak

#include <iostream>
#include <vector>
#include <immintrin.h>
#include "math.h"

using namespace rt;

int main() {

  Vector3<SimdFloat8> x(123);
  Vector3<SimdFloat8> y(321);

  auto z = x + y;
  auto t = z * x;
  
  std::cout << t << std::endl;
  return 0;
}
