// Realistyczna Grafika Komputerowa 2015/2016
// Krzysztof Chrobak
#pragma once

#include <vector>

#include "primitives.h"

namespace rt {

  struct Resolution {
    int width;
    int height;

    Resolution() {}

    Resolution(int width_, int height_) 
      : width(width_), height(height_)
    {}

    int get_num_pixels() const {
      return width * height;
    }

    float get_width_to_height() const {
      return width / (float) height; 
    }
  };

  std::ostream& operator << (std::ostream& out, const Resolution& resolution);

  struct Camera {
    Resolution resolution;
    V location;
    V look_at;
    V up;
    float sensor_height_to_focal;

    Camera() {}

    Camera(
        Resolution resolution_,
        V location_,
        V look_at_,
        V up_,
        float sensor_height_to_focal_) :
      resolution(resolution_),
      location(location_),
      look_at(look_at_),
      up(up_),
      sensor_height_to_focal(sensor_height_to_focal_)
    {}

    std::vector<Ray> get_rays() const;
  
    void save_exr(
        const std::vector<V>& colors, 
        const std::string& filename) const;
  };

  std::ostream& operator << (std::ostream& out, const Camera& camera);
}
