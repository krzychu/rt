// Realistyczna Grafika Komputerowa 2015/2016
// Krzysztof Chrobak

#include "math.h"
#include <algorithm>
#include <random>


namespace rt {

  float saturate(float x, float x_min, float x_max) {
    return std::min(std::max(x, x_min), x_max);
  }

  std::ostream& operator<< (std::ostream& out, const SimdFloat8& x) {
    float xs[8];
    x.store(xs);
    out << "<";
    for (int i = 0; i < 8; i++) {
      out << xs[i];

      if (i != 7)
        out << " ,";
      else
        out << ">";
    }

    return out;
  }

  SV8 load(const V8& v) {
    SV8 result;
    result.x.load(v.x);
    result.y.load(v.y);
    result.z.load(v.z);
    return result;
  }

  V8 store(const SV8& v) {
    V8 result;
    v.x.store(result.x);
    v.y.store(result.y);
    v.z.store(result.z);
    return result;
  }

  V min(const V& u, const V& v) {
    V result;
    result.x = std::min(u.x, v.x);
    result.y = std::min(u.y, v.y);
    result.z = std::min(u.z, v.z);
    return result;
  }

  V max(const V& u, const V& v) {
    V result;
    result.x = std::max(u.x, v.x);
    result.y = std::max(u.y, v.y);
    result.z = std::max(u.z, v.z);
    return result;
  }

  int argmax(const V& v) {
    float length = 0;
    int longest = 0;
    for (int i = 0; i < 3; i++) {
      if (length < v[longest]) {
        longest = i;
        length = v[longest];
      } 
    }
    return longest;
  }
  
  float randu() {
    static std::random_device rd;
    static std::mt19937 gen(rd());
    static std::uniform_real_distribution<float> dist;
    return dist(gen);
  }
}
