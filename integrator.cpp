// Realistyczna Grafika Komputerowa 2015/2016
// Krzysztof Chrobak

#include "integrator.h"
#include "moller.h"
#include "stats.h"


namespace rt {

  bool Integrator::intersect(
      const Ray& ray,
      Intersection* intersection,
      Material* material,
      V* point) const {
  
    *intersection = accelerator->intersect(ray);
    if (intersection->tag == Intersection::NO_TAG) 
      return false;

    intersection->normal = intersection->normal.normalized();
    auto& object = scene->model->objects[intersection->tag];

    *material = scene->model->materials[object->material_name];
    *point = ray(intersection->t);
    return true;
  }


  V Integrator::get_light_contribution(
      const Light& light, 
      const Ray& ray,
      V point, 
      V normal,
      const Material& material) const {
    
    const auto sample = light.sample(point);
    assert(sample.probability >= 0);
    assert(sample.radiance.nonnegative());

    if (sample.probability == 0 || sample.radiance.is_zero())
      return V(0);

    assert(sample.to_light.origin == point);
    ASSERT_UNIT(sample.to_light.direction);

    const V to_light = sample.to_light.direction;
    const float fd = diffuse->f(material, normal, ray.direction, to_light);
    const float fs = specular->f(material, normal, ray.direction, to_light);
    
    const float cos_theta = normal.dot(to_light);
    if (cos_theta <= 0)
      return V(0);
  
    stats::add_shadow_ray();
    if (accelerator->does_intersect(sample.to_light, sample.distance))
      return V(0);
  
    const V f = 0.5f * fd * material.diffuse + fs * material.specular;
    return (cos_theta / sample.probability) * f * sample.radiance;
  }

  
  V Integrator::get_all_lights_contributions(
      const Ray& ray,
      V point, 
      V normal,
      const Material& material) const {

    V radiance = V(0);
    for (const auto& light : scene->lights) {
      const auto contribution = get_light_contribution(
          *light, ray, point, normal, material);
      
      assert(contribution.nonnegative());
      radiance = radiance + contribution;
    }
    return radiance;
  }

  
  V WhittedIntegrator::integrate(const Ray& ray, int level) const {
    if (level > max_level)
      return V(0, 0, 0);

    stats::add_ray(level);

    Intersection closest;
    V point;
    Material material;

    if (!intersect(ray, &closest, &material, &point))
      return V(0, 0, 0);

    V radiance = material.emissive * (-closest.normal.dot(ray.direction));

    radiance = radiance + get_all_lights_contributions(
        ray, point, closest.normal, material);
    
    if (material.specular.is_zero())
      return radiance;

    const auto sample = specular->sample(material, closest.normal, ray.direction);
    Ray reflected_ray;
    reflected_ray.origin = point;
    reflected_ray.direction = sample.direction;
    const V incoming_radiance = integrate(reflected_ray, level + 1);

    const float cos_theta = closest.normal.dot(sample.direction);
    const float p = sample.probability;

    radiance = radiance + sample.f * (cos_theta / p) * material.specular * incoming_radiance;

    return radiance;
  }


  V DirectIntegrator::integrate(const Ray& ray, int level) const {
    stats::add_ray(level);

    Intersection intersection;
    Material material;
    V point;

    if (!intersect(ray, &intersection, &material, &point))
      return V(0, 0, 0);

    V radiance = material.emissive * INVPI;
    radiance = radiance + get_all_lights_contributions(
        ray, point, intersection.normal, material);

    return radiance;
  }

  
  V PathIntegrator::integrate(const Ray& ray, int level) const {
    V throughput = V(1.0);
  
    V radiance = V(0);
    Ray current = ray;
    for (level = 0; level < 10; level++) {
      stats::add_ray(level);

      if (dbg::is_debug_pixel()) {
        std::cout << "---------------------------------" << std::endl;
        std::cout << "level = " << level << std::endl; 
      }

      Intersection intersection;
      Material material;
      V point;
      if (!intersect(current, &intersection, &material, &point))
        break;
    
      assert(throughput.nonnegative());

      if (level == 0)
        radiance = radiance + throughput * material.emissive * INVPI;
      
      assert(radiance.nonnegative());

      // path ending here - we sample all lights
      const auto all = get_all_lights_contributions(
          current, point, intersection.normal, material);

      radiance = radiance + throughput * all;

      assert(radiance.nonnegative());

      const float se = exp(material.specular.norm());
      const float de = exp(material.diffuse.norm());
      const float specular_probability = se / (se + de);

      // another bounce
      const BRDF* brdf;
      float pdf;
      V color;
      if (randu() < specular_probability) {
        brdf = specular;
        color = material.specular;
        pdf = specular_probability;
      } else {
        brdf = diffuse;
        color = material.diffuse;
        pdf = 1.0f - specular_probability;
      }

      const auto brdf_sample = brdf->sample(
          material, intersection.normal, current.direction);
      
      const float costheta = intersection.normal.dot(brdf_sample.direction);
      
      throughput = 
        0.5f * costheta / (pdf * brdf_sample.probability) * throughput * brdf_sample.f * color;

      if (throughput.is_zero())
        break;

      current.origin = point;
      current.direction = brdf_sample.direction;

      const float continuation_probability = std::min(throughput.norm(), 0.5f);
      if (randu() < continuation_probability)
        throughput = (1.0f / continuation_probability) * throughput;
      else
        break;
    }

    return (1.f / (level + 1)) * radiance;
  }
}
