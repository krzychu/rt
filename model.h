// Realistyczna Grafika Komputerowa 2015/2016
// Krzysztof Chrobak
#pragma once

#include <iostream>
#include <memory>
#include <vector>
#include <unordered_map>

#include "math.h"
#include "primitives.h"

namespace rt {

  const int MISSING = -1;

  struct FaceNode {
    int v = MISSING; // vertex
    int t = MISSING; // texture coord
    int n = MISSING; // normal
  };

  std::ostream& operator << (std::ostream& out, const FaceNode& node);

  struct Material {
    V ambient;
    V diffuse;
    V specular;
    V emissive;
    float specular_exponent;
    float refraction_index = 1.0f;
    float reflectivity;
  };

  std::ostream& operator << (std::ostream& out, const Material& material);

  std::string strip(const std::string str);

  std::string get_other_file(
      const std::string& other_file, 
      const std::string& base_path);

  using MaterialMap = std::unordered_map<std::string, Material>;

  MaterialMap read_mtl(std::istream& in);

  MaterialMap read_mtl_from_file(const std::string& mtl_path);

  struct Object {
    std::string name;
    std::string material_name;
    std::vector<std::vector<FaceNode>> faces;

    Object(const std::string& name_) : name(name_) {}
  };

  std::ostream& operator << (std::ostream& out, const Object& object);

  struct Model {
    std::vector<V> vertices;
    std::vector<V> normals;
    MaterialMap materials;
    std::vector<std::unique_ptr<Object>> objects;
  };

  std::ostream& operator << (std::ostream& out, const Model& model);
  
  std::unique_ptr<Model> read_obj(
      std::istream& in,
      const std::string& obj_path = "");

  std::unique_ptr<Model> read_obj_from_file(const std::string& obj_path);

  std::vector<Triangle> get_triangles(
      int id, const Model& model, const Object& object);
}
