// Realistyczna Grafika Komputerowa 2015/2016
// Krzysztof Chrobak
#pragma once

#include <vector>
#include <memory>
#include <iostream>

#include "camera.h"
#include "light.h"
#include "model.h"
#include "primitives.h"

namespace rt {

  struct Scene {
    int recursion_level;
    std::string output_path;
    std::unique_ptr<Camera> camera;
    std::vector<std::unique_ptr<Light>> lights; 
    std::unique_ptr<Model> model;
  };

  std::ostream& operator << (std::ostream& out, const Scene& scene);

  typedef std::unique_ptr<Model> (*OBJReader) (const std::string&);
  
  std::unique_ptr<Scene> read_rgk(
      std::istream& in,
      const std::string& rgk_path,
      OBJReader obj_reader);

  std::unique_ptr<Scene> read_rgk_from_file(
      const std::string& rgh_path);

  std::vector<Triangle> get_triangles(const Scene& scene);
}
