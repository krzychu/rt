#!/bin/bash

time ./raytracer --scene $1 --accelerator SimdBVH --bvh_leaf_threshold 8 --samples_per_pixel 512
