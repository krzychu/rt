// Realistyczna Grafika Komputerowa 2015/2016
// Krzysztof Chrobak

#include <string>
#include <fstream>
#include <exception>

#include "scene.h"

namespace rt {

  std::unique_ptr<Scene> read_rgk_from_file(
      const std::string& rgk_path) {
    std::ifstream fs(rgk_path);
    if (!fs) {
      throw std::runtime_error("Unable to open: " + rgk_path);
    }

    return read_rgk(fs, rgk_path, read_obj_from_file);
  }


  std::unique_ptr<Scene> read_rgk(
      std::istream& in,
      const std::string& rgk_path,
      OBJReader obj_reader) {

    std::unique_ptr<Scene> scene(new Scene);
    std::string obj_file_name;
    // ommit comment
    getline(in, obj_file_name);
    getline(in, obj_file_name);

    obj_file_name = get_other_file(obj_file_name, rgk_path);

    scene->model = obj_reader(obj_file_name);

    // ommit output file name
    getline(in, scene->output_path);
    scene->output_path = strip(scene->output_path);

    in >> scene->recursion_level;

    scene->camera = std::unique_ptr<Camera>(new Camera());
    in 
      >> scene->camera->resolution.width 
      >> scene->camera->resolution.height;
    
    in >> scene->camera->location;
    in >> scene->camera->look_at;
    in >> scene->camera->up;
    in >> scene->camera->sensor_height_to_focal;
    
    while (true) {
      std::string command; 
      in >> command;

      if (command == "")
        break;

      if (command == "L") {
        std::unique_ptr<PointLight> light(new PointLight);
        in >> light->location >> light->color >> light->energy;
        light->color = (1 / 255.f) * light->color;
        scene->lights.push_back(std::move(light));
      }
      else {
        throw std::runtime_error("Unknown scene file command: " + command);
      }
    }
    
    int object_id = 0;
    for (const auto& object : scene->model->objects) {
      const auto& mt = scene->model->materials[object->material_name];
      if (!mt.emissive.is_zero()) {
        std::unique_ptr<MeshLight> light(
            new MeshLight(
              object_id,
              mt.emissive, 
              get_triangles(object_id, *scene->model, *object)));
        scene->lights.push_back(std::move(light));
      }
      object_id++;
    }

    return scene;
  }

  std::ostream& operator << (std::ostream& out, const Scene& scene) {
    out << "[[SCENE]]" << std::endl;
    out << "recursion_level = " << scene.recursion_level << std::endl;
    out << "camera:" << std::endl << *scene.camera;
    out << "model:" << *scene.model; 

    return out;
  }

  std::vector<Triangle> get_triangles(const Scene& scene) {
    std::vector<Triangle> triangles;
    int object_id = 0;
    for (const auto& object : scene.model->objects) {
      const auto object_triangles = get_triangles(
          object_id++, *scene.model, *object);
    
      std::copy(
          object_triangles.begin(), 
          object_triangles.end(), 
          std::back_inserter(triangles));
    }
    return triangles;
  }
}
