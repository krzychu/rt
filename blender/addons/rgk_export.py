bl_info = {
    "name": "RGK Format",
    "author": "Krzysztof Chrobak",
    "blender": (2, 69, 0),
    "location": "File > Export"
}

'''
RGK scene exporter
'''

import bpy
import rgk

from bpy.props import IntProperty

from bpy_extras.io_utils import (
    ExportHelper,
    path_reference_mode,
    axis_conversion,
)

class ExportRGK(bpy.types.Operator, ExportHelper):
    bl_idname = "export_scene.rgk"
    bl_label = 'Export RGK'
    bl_options = {'PRESET'}

    filename_ext = ".rgk"

    recurson_level = IntProperty(
        name="Recursion Level",
        description="",
        default=1
    )

    def execute(self, context):
        keywords = self.as_keywords()
        return rgk.export_rgk_scene(self.filepath, self.recurson_level)
        


def menu_func_export(self, context):
    self.layout.operator(ExportRGK.bl_idname, text="RGK (.rgk)")


def register():
    bpy.utils.register_module(__name__)
    bpy.types.INFO_MT_file_export.append(menu_func_export)


def unregister():
    bpy.utils.unregister_module(__name__)
    bpy.types.INFO_MT_file_export.remove(menu_func_export)


if __name__ == "__main__":
    register()
