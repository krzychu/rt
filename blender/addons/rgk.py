import bpy
import math
import mathutils
import os.path


def get_vector_string(v):
    return ' '.join(map(str, v[:3]))


def write_vector(f, v):
    f.write(get_vector_string(v) + '\n')


def get_name(path):
    path, _ = os.path.splitext(path)
    return os.path.split(path)[-1]


def export_rgk_metadata(f, obj_path, recursion_level):
    f.write('Exported from Blender\n')
    f.write(obj_path + '\n')
    f.write(get_name(obj_path) + ".exr\n")

    rx = bpy.data.scenes[0].render.resolution_x
    ry = bpy.data.scenes[0].render.resolution_y

    f.write('{0}\n{1} {2}\n'.format(recursion_level, rx, ry))

    assert len(bpy.data.cameras) == 1
    camera = bpy.data.cameras[0]

    assert camera.lens_unit == 'MILLIMETERS'
    assert camera.type == 'PERSP'

    camera_object = None
    for obj in bpy.data.objects:
        if obj.data == camera:
            camera_object = obj

    assert camera_object

    conv = mathutils.Matrix.Rotation(math.radians(-90), 4, 'X')

    tr = conv * camera_object.matrix_world

    view_point = tr * mathutils.Vector((0, 0, 0, 1))
    write_vector(f, view_point)

    look_at = tr * mathutils.Vector((0, 0, -1, 1))
    write_vector(f, look_at)

    up = tr * mathutils.Vector((0, 1, 0, 0))
    write_vector(f, up)

    sx = camera.sensor_width
    sy = sx * ry / float(rx)

    yview = sy / camera.lens
    f.write('{0}\n'.format(yview))

    for obj in bpy.data.objects:
        if obj.type != 'LAMP':
            continue

        f.write('L {0} {1} {2}\n'.format(
            get_vector_string(conv * obj.location),
            get_vector_string(obj.data.color),
            obj.data.energy * 100.0
        ))


def export_rgk_scene(rgk_path, recursion_level):
    name = get_name(rgk_path)
    obj_path = name + '.obj'

    with open(rgk_path, 'w') as f:
        export_rgk_metadata(
            f,
            obj_path,
            recursion_level=1
        )

    bpy.ops.export_scene.obj(
        filepath=os.path.join(os.path.dirname(rgk_path), obj_path),
        use_triangles=True,
        use_normals=True,
        axis_forward='-Z',
        axis_up='Y'
    )
    return {'FINISHED'}


if __name__ == '__main__':
    export_rgk_scene('test.rgk', 1)
