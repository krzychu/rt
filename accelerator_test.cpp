// Realistyczna Grafika Komputerowa 2015/2016
// Krzysztof Chrobak

#include <gtest/gtest.h>
#include "accelerator.h"

using namespace rt;


TEST(BVHTest, struct_sizes) {
  ASSERT_EQ(9 * 4, sizeof(BVHNode));
}
