# Raytracer

## Obtaining Source and Time Travel
```
git clone https://krzychu@bitbucket.org/krzychu/rt.git
git checkout [tag_name]
```
Substitute `tag_name` with project stage, for example:

* task\_1 
* task\_2 
* task\_3

## Dependencies
Before compiling, make sure that following libraries are installed:

* OpenEXR

Your GCC version must support C++11.

## Building
```
mkdir build
cd build
cmake ..
make -j 4
```

## Usage

Successful build process should produce `raytracer` binary, and `RT.sh` script, which
sets sensible flag values. It can be used like this:
```
./RT.sh scene_file
```

Output image is in EXR format, so it can be viewed with:
```
exrviewer scene.exr
```

`raytracer` binary takes following arguments:

* `--scene` scene to render
* `--accelerator` acceleration structure to use:
    * `BVH` bounding volume hierarchy
    * `SimdBVH` bounding volume hierarchy, with SIMD leaf intersections 
    * `List` list of triangles
    * `SimdList` list of triangles, but with SIMD intersections
* `--samples_per_pixel` number of samples per pixel
* `--integrator` integrator to use, either `Whitted`, `Direct` or `Path` (default)
* `--specular` BRDF to use for specular reflections
* `--diffuse` diffuse BRDF

### BRDFs

You can choose BRDFs from:

* `Lambert` (default for diffuse reflection)
* `Torrance` (default for specular reflection)
* `Phong` 
* `Zero` (no reflection at all)

## Paths

**ALL** paths are relative to file in which they occur.

## Scene Export

Instead of rolling my own 3D modeling software I implemented an add-on for
Blender. It exports both the triangulated OBJ model and scene description  that
can be consumed by this raytracer.

To export scene from command line as `test.rgk`, you can use following command:
```
blender scene.blend -b --python blender/addons/rgk.py
```

### Add-on Activation

Blender needs to be able to find the add-on, so you should put path to the `blender` directory in "Scripts" section of user preferences.

![preferences](img/blender_settings.png)

### Add-on Usage

Just click `File > Export > RGK (*rgk)`, set the recursion level and you are done.

![usage](img/export.png)

Exporter makes the following assumptions about the camera:

* There is only one
* Lens unit is millimeters
* Type is "perspective"

Violating them would cause an error.

### Exported Objects

Following Blender scene elements are exported:

* Camera
  * Position
  * Orientation
  * Focal length
* Rendered image resolution
* Objects (materials supported to the extent OBJ exporter supports them)
* Lights
  * Position
  * Color
  * Energy
