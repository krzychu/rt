// Realistyczna Grafika Komputerowa 2015/2016
// Krzysztof Chrobak

#include <gtest/gtest.h>
#include "scene.h"
#include <string>
#include <sstream>


using namespace rt;

const std::string text = R"(comment line
keks.obj
keks.png
3
10 20
1 2 3
4 5 6
7 8 9
0.456
L 1 2 3 255.0 100 150 100
)";


std::unique_ptr<Model> mock_reader(const std::string&) {
  return nullptr;
}


TEST(SceneTest, reads_rgk) {
  std::stringstream in(text);

  auto scene = read_rgk(in, "", mock_reader);
  
  ASSERT_EQ(3, scene->recursion_level);
  ASSERT_EQ(10, scene->camera->resolution.width);
  ASSERT_EQ(20, scene->camera->resolution.height);

  ASSERT_EQ(V(1, 2, 3), scene->camera->location);
  ASSERT_EQ(V(4, 5, 6), scene->camera->look_at);
  ASSERT_EQ(V(7, 8, 9), scene->camera->up);

  ASSERT_FLOAT_EQ(0.456, scene->camera->sensor_height_to_focal); 

  ASSERT_EQ(nullptr, scene->model);

  ASSERT_EQ(1, scene->lights.size());
  
  PointLight* light = static_cast<PointLight*>(scene->lights[0].get());
  ASSERT_EQ(V(1, 2, 3), light->location);
  ASSERT_FLOAT_EQ(1, light->color[0]);
  ASSERT_FLOAT_EQ(0.392157, light->color[1]);
  ASSERT_FLOAT_EQ(0.58823532, light->color[2]);
  ASSERT_EQ(100, light->energy);
}


TEST(SceneTest, does_not_hang_on_nonexistent_obj) {
  std::stringstream in(text);
  ASSERT_THROW(read_rgk(in, "", read_obj_from_file), std::runtime_error);
}
