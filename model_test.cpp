// Realistyczna Grafika Komputerowa 2015/2016
// Krzysztof Chrobak

#include <gtest/gtest.h>
#include <sstream>

#include "model.h"

using namespace rt;

TEST(ModelTest, get_other_file) {
  ASSERT_EQ("ab.c", get_other_file("ab.c", ""));
  ASSERT_EQ("ab.c", get_other_file("ab.c", "kek.obj"));
  ASSERT_EQ("a/b/c/ab.c", get_other_file("ab.c", "a/b/c/kek.obj"));
}


TEST(ModelTest, reads_vertices) {
  std::stringstream in("v 1 2 3");

  auto model = read_obj(in);
  ASSERT_EQ(1, model->vertices.size());
  ASSERT_EQ(0, model->normals.size());
  ASSERT_EQ(V(1, 2, 3), model->vertices[0]);
}


TEST(ModelTest, reads_normals) {
  std::stringstream in("vn 1 2 3");

  auto model = read_obj(in);
  ASSERT_EQ(0, model->vertices.size());
  ASSERT_EQ(1, model->normals.size());
  ASSERT_EQ(V(1, 2, 3), model->normals[0]);
}


TEST(ModelTest, ignores_unknown) {
  std::stringstream in(R"(
    # comment
    v 1 2 3
    b ua sxx
    vn 4 5 6
    )");

  auto model = read_obj(in);
  ASSERT_EQ(1, model->vertices.size());
  ASSERT_EQ(1, model->normals.size());
  ASSERT_EQ(V(1, 2, 3), model->vertices[0]);
  ASSERT_EQ(V(4, 5, 6), model->normals[0]);
}


TEST(ModelTest, reads_faces) {
  std::stringstream in(R"(
    f 1 2 3 5 6
    f 1/2/3
    f 1/2
    f 1//2
    )");

  auto model = read_obj(in);
  ASSERT_EQ(1, model->objects.size());

  const auto& object = model->objects[0];
  ASSERT_EQ(4, object->faces.size());

  auto face = object->faces[0];
  ASSERT_EQ(5, face.size());
  ASSERT_EQ(0, face[0].v);
  ASSERT_EQ(MISSING, face[0].t);
  ASSERT_EQ(MISSING, face[0].n);

  face = object->faces[1];
  ASSERT_EQ(1, face.size());
  ASSERT_EQ(0, face[0].v);
  ASSERT_EQ(1, face[0].t);
  ASSERT_EQ(2, face[0].n);

  face = object->faces[2];
  ASSERT_EQ(1, face.size());
  ASSERT_EQ(0, face[0].v);
  ASSERT_EQ(1, face[0].t);
  ASSERT_EQ(MISSING, face[0].n);

  face = object->faces[3];
  ASSERT_EQ(1, face.size());
  ASSERT_EQ(0, face[0].v);
  ASSERT_EQ(MISSING, face[0].t);
  ASSERT_EQ(1, face[0].n);
}

TEST(ModelTest, reads_materials) {
  std::stringstream in(R"(
    # hehe

    newmtl Ceramiks_y
    Ns 96.078431
    Ka 0.000000 0.000000 0.000000
    Kd 0.765490 0.762353 0.787451
    Ks 0.043137 0.043137 0.043137
    Ni 1.000000
    d 1.000000
    illum 2

    newmtl Standard_3
    Ns 96.078431
    Ka 0.000000 0.000000 0.000000
    Kd 0.467451 0.467451 0.467451
    Ks 0.449020 0.449020 0.449020
    Ke 100 200 300
    Ni 1.000000
    d 1.000000
    illum 2
    map_Kd CM.jpg)");

  auto map = read_mtl(in);
  ASSERT_EQ(2, map.size());

  auto ceramiks = map["Ceramiks_y"];
  ASSERT_EQ(V(0, 0, 0), ceramiks.ambient);
  ASSERT_EQ(V(0.765490, 0.762353, 0.787451), ceramiks.diffuse);
  ASSERT_EQ(V(0.043137, 0.043137, 0.043137), ceramiks.specular);
  ASSERT_FLOAT_EQ(96.078431, ceramiks.specular_exponent);
  ASSERT_FLOAT_EQ(0, ceramiks.reflectivity);

  auto standard = map["Standard_3"];
  ASSERT_EQ(V(0, 0, 0), standard.ambient);
  ASSERT_EQ(V(0.467451, 0.467451, 0.467451), standard.diffuse);
  ASSERT_EQ(V(0.449020, 0.449020, 0.449020), standard.specular);
  ASSERT_EQ(V(100, 200, 300), standard.emissive);
  ASSERT_FLOAT_EQ(96.078431, standard.specular_exponent);
  ASSERT_FLOAT_EQ(0, standard.reflectivity);
}

TEST(ModelTest, reads_model_from_file) {
  auto model = read_obj_from_file("simple/simple.obj");

  ASSERT_EQ(1, model->materials.size());
  auto material = model->materials["Material"];
  
  ASSERT_EQ(V(0, 0, 0), material.ambient);
  ASSERT_EQ(V(0.640000, 0.640000, 0.640000), material.diffuse);
  ASSERT_EQ(V(0.500000, 0.500000, 0.500000), material.specular);

  ASSERT_EQ(1, model->objects.size());
  const auto& object = model->objects[0];

  ASSERT_EQ("Material", object->material_name);
  ASSERT_EQ(12, object->faces.size());
}
