// Realistyczna Grafika Komputerowa 2015/2016
// Krzysztof Chrobak
#pragma once

#include "primitives.h"

namespace rt {
  namespace moller {

    using TriItr = std::vector<Triangle>::const_iterator;

    const float EPSILON = 1e-2;
    
    void intersect(
        Intersection& result,
        const Ray& ray,
        const Triangle8& triangle);


    void intersect(
        Intersection& result,
        const Ray& ray, 
        const Triangle& triangle);


    template<class T>
    bool does_intersect(
        const Ray& ray,
        const T& triangle,
        float max_distance) {
    
      Intersection result;
      intersect(result, ray, triangle);
      return result.tag != Intersection::NO_TAG && result.t < max_distance - EPSILON;
    }
  }
}
