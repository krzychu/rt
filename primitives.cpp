// Realistyczna Grafika Komputerowa 2015/2016
// Krzysztof Chrobak

#include "primitives.h"
#include <mm_malloc.h>

namespace rt {

  const uint32_t Intersection::NO_TAG = -1;
  
  float Triangle::area() const {
    return 0.5f * (b - a).cross(c - a).norm();  
  }


  TriangleSample Triangle::sample() const {
    float alpha = randu();  
    float beta = randu();
    if (alpha + beta > 1) {
      alpha = 1 - alpha;
      beta = 1 - beta;
    }
    
    TriangleSample sample;
    sample.point = alpha * (b - a) + beta * (c - a) + a;
    sample.normal = 
      (alpha * bn + beta * cn + (1 - alpha - beta) * an).normalized();
    return sample;
  }

  
  std::ostream& operator<< (
      std::ostream& out, const Intersection& intersection) {
    
    out 
      << "Intersection(t=" << intersection.t 
      << ", normal=" << intersection.normal << ")";

    return out;
  }

  
  void V8::set(int i, const V& v) {
    x[i] = v.x;
    y[i] = v.y;
    z[i] = v.z;
  }


  Triangle8::Triangle8(TriItr begin, TriItr end) {
    const size_t num_triangles = end - begin;
    assert(num_triangles <= 8);

    std::fill(tag, tag + V8::SIZE, Intersection::NO_TAG);
    int tri = 0;
    while (begin != end) {
      a.set(tri, begin->a);
      b.set(tri, begin->b);
      c.set(tri, begin->c);

      an.set(tri, begin->an);
      bn.set(tri, begin->bn);
      cn.set(tri, begin->cn);

      tag[tri] = begin->tag;
      begin++;
      tri++;
    }
  }


  Triangle8::Triangle8() {
    std::fill(tag, tag + V8::SIZE, Intersection::NO_TAG);
  }


  AABB bound(const AABB& x, const AABB& y) {
    AABB result;
    result.lower = min(x.lower, y.lower);
    result.upper = max(x.upper, y.upper);
    return result;
  }


  AABBIntersection AABB::intersect(const Ray& ray) const {
    AABBIntersection result;
    
    const V l = (lower - ray.origin) / ray.direction;
    const V u = (upper - ray.origin) / ray.direction;

    for (int axis = 0; axis < 3; axis++) {
      float min_t = l[axis], max_t = u[axis];
      if (ray.direction[axis] < 0)
        std::swap(min_t, max_t);

      if (std::isnan(min_t)) 
        min_t = Traits<float>::neg_inf();
      
      if (std::isnan(max_t))
        max_t = Traits<float>::pos_inf();

      result.min_t = std::max(min_t, result.min_t);
      result.max_t = std::min(max_t, result.max_t);
    }
    return result;
  }


  std::ostream& operator << (std::ostream& out, const AABB& aabb) {
    out << "AABB(lower=" << aabb.lower << ", upper=" << aabb.upper << ")"; 
    return out;
  }


  Triangle8Array::Triangle8Array(int num_triangles_) {
    num_triangles = num_triangles_;
    triangles = (Triangle8*)_mm_malloc(sizeof(Triangle8) * num_triangles, 32);
    new (triangles) Triangle8[num_triangles];
  }

  
  Triangle8Array::~Triangle8Array() {
    _mm_free(triangles);
  }


  Triangle8Array::Triangle8Array(const Triangle8Array& other) {
    num_triangles = other.num_triangles;
    triangles = (Triangle8*)_mm_malloc(sizeof(Triangle8) * num_triangles, 32);
    new (triangles) Triangle8[num_triangles];
    std::copy(other.cbegin(), other.cend(), triangles);
  }


  const Triangle8Array& Triangle8Array::operator= (const Triangle8Array& other) {
    num_triangles = other.num_triangles;
    triangles = (Triangle8*)_mm_malloc(sizeof(Triangle8) * num_triangles, 32);
    std::copy(other.cbegin(), other.cend(), triangles);
    return other;
  }
}
