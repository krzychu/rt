// Realistyczna Grafika Komputerowa 2015/2016
// Krzysztof Chrobak 

#include "brdf.h"
#include <cassert>

namespace rt {

  BRDFSample Phong::sample(
      const Material& material, const V& normal, const V& in) const {
    const V reflected = in - 2 * (in.dot(normal)) * normal;
    BRDFSample sample;
    sample.f = f(material, normal, in, reflected);
    sample.direction = reflected;
    sample.probability = 1.0f;
    return sample;
  }


  float Phong::f(
      const Material& material, const V& normal, const V& in, const V& out) const {
    const V half = (out - in).normalized();
    const float nh = clamp(normal.dot(half));
    const float r = pow(nh, material.specular_exponent);
    return r;
  }


  V get_rotated_normal(const V& normal, float cos_theta) {
    ASSERT_UNIT(normal);
    const float phi = 2 * M_PI * randu();
    const float sin_theta = sqrt(1 - cos_theta * cos_theta);
    
    const V in = V(1, 1, 1);
    const V a = normal.cross(in).normalized();
    const V b = normal.cross(a);
    assert(fabs(normal.dot(a)) < 1e-3);
    assert(fabs(normal.dot(b)) < 1e-3);
    assert(fabs(a.dot(b)) < 1e-3);

    const V result = 
      normal * cos_theta + a * sin_theta * cos(phi) + b * sin_theta * sin(phi);

    assert(fabs(result.dot(normal) - cos_theta) < 1e-3);

    ASSERT_UNIT(result);
    return result;
  }


  BRDFSample Lambert::sample(
      const Material& material, const V& normal, const V& in) const {
    
    const float cos_theta = sqrt(1 - randu());
    BRDFSample sample;
    sample.f = INVPI;
    sample.direction = get_rotated_normal(normal, cos_theta);
    sample.probability = 1.0f / cos_theta;
    return sample;
  }
  
  
  float Lambert::f(
      const Material& material, const V& normal, const V& in, const V& out) const {
    return INVPI;
  }


  BRDFSample Zero::sample(
      const Material& material, const V& normal, const V& in) const {
    BRDFSample sample;
    sample.f = 0;
    sample.direction = normal;
    sample.probability = 1.0;
    return sample;
  }
  
  
  float Zero::f(
      const Material& material, const V& normal, const V& in, const V& out) const {
    return 0;
  }


  float Torrance::blinn_density(const Material& material, float nh) const {
    const float e = material.specular_exponent;
    return INVPI * 0.5 * (e + 1) * pow(clamp(nh), e);
  }


  BRDFSample Torrance::sample(
      const Material& material, const V& normal, const V& in) const {
    ASSERT_UNIT(normal);
    ASSERT_UNIT(in);
    
    const float cos_theta = pow(randu(), 1.0f / (material.specular_exponent + 1));
    const V h = get_rotated_normal(normal, cos_theta);
    ASSERT_UNIT(h);
    assert(fabs(h.dot(normal) - cos_theta) < 1e-3);

    const V reflected = (in - 2 * (in.dot(h)) * h).normalized();
    ASSERT_UNIT(reflected);

    const float hv = clamp(-h.dot(in));
    assert(hv <= 1.0f);

    BRDFSample sample;
    sample.f = f(material, normal, in, reflected);
    sample.direction = reflected;
    sample.probability = blinn_density(material, cos_theta) / (4.f * hv);
    return sample;
  }

  
  float Torrance::f(
      const Material& material, const V& normal, const V& in, const V& out) const {

    const V h = (out - in).normalized();
    const float cos_theta = -h.dot(in);

    const float nh = h.dot(normal);
    const float nv = -in.dot(normal);
    const float nl = out.dot(normal);

    if (nh <= 0 || nv <= 0 || nl <= 0 || cos_theta <= 0)
      return 0;

    const float G = std::min(1.f, 2.f * nh / cos_theta * std::min(nv, nl));
    const float d = (material.refraction_index == 1) ? 2.0f : material.refraction_index;
    const float Rsq = (d - 1) / (1 + d);
    const float R = clamp(Rsq * Rsq);
    const float F = R + (1 - R) * pow(clamp(1.f - cos_theta), 5.f);

    const float D = blinn_density(material, nh);
    const float f = D * G * F / (4 * nv * nl);

    assert(f >= 0.0f);
    assert(!std::isinf(f));
    return f;
  }
}
