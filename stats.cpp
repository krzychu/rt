// Realistyczna Grafika Komputerowa 2015/2016
// Krzysztof Chrobak

#include "stats.h"
#include <inttypes.h>
#include <chrono>
#include <cassert>

namespace rt {
  namespace stats {
    const int max_level = 100;
    uint64_t num_rays[max_level];
    uint64_t num_shadow_rays = 0;
    std::chrono::steady_clock::time_point tic_begin;

    void init() {
      std::fill(num_rays, num_rays + max_level, 0);
    }

    void add_ray(int level) { 
      assert(level < max_level);
      num_rays[level]++; 
    }

    void add_shadow_ray() {
      num_shadow_rays++;
    }

    void tic() {
      tic_begin = std::chrono::steady_clock::now();
    }

    void toc(std::ostream& out, const std::string& label) {
      auto end = std::chrono::steady_clock::now();
      using s = std::chrono::seconds;

      int sec = std::chrono::duration_cast<s>(end - tic_begin).count();
      if (sec) {
        out << label << " took " << sec << " s" << std::endl;
        return;
      }

      using ms = std::chrono::milliseconds;
      int msec = std::chrono::duration_cast<ms>(end - tic_begin).count();
      if (msec) {
        out << label << " took " << msec << " ms" << std::endl;
      }
    }

    void print(std::ostream& out) {
      out << "------------------------" << std::endl;
      out << "         Stats          " << std::endl;
      out << "------------------------" << std::endl;
      out << "Following numbers are APPROXIMATE" << std::endl;
      out << "Number of rays:" << std::endl;
      for (int i = 0; i < max_level; i++) {
        if (!num_rays[i])
          break;

        out << i << "  " << num_rays[i] << std::endl;
      }
      out << "Number of shadow rays: " << num_shadow_rays << std::endl;
    }
  }

  namespace dbg {
    int current_pixel;
    int debug_pixel;

    void set_current_pixel(int num) {
      current_pixel = num;
    }

    void set_debug_pixel(int num) {
      debug_pixel = num;
    }

    bool is_debug_pixel() {
#ifdef NDEBUG
      return false;
#else
      return debug_pixel == current_pixel;
#endif
    }
  }
}
