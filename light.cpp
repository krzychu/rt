// Realistyczna Grafika Komputerowa 2015/2016
// Krzysztof Chrobak

#include "light.h"

namespace rt {

  LightSample PointLight::sample(V point) const {
    const V to = location - point;
    const float inv_sq_dist = 1.0f / to.dot(to);

    Ray to_light;
    to_light.origin = point;
    to_light.direction = to.normalized();

    LightSample sample;
    sample.radiance = energy * inv_sq_dist * color;
    sample.to_light = to_light;
    sample.probability = INVPI;
    sample.distance = to.norm();
    return sample;
  }


  std::ostream& operator << (std::ostream& out, const PointLight& light) {
    out << "[[LIGHT]]" << std::endl;
    out << "location = " << light.location << std::endl;
    out << "color = " << light.color << std::endl;
    out << "energy = " << light.energy << std::endl;
    return out;
  }


  MeshLight::MeshLight(
      int object_id_,
      const V& emissive_, 
      const std::vector<Triangle>& triangles_) 
  : Light(object_id_), emissive(emissive_), triangles(triangles_) {

    cdf.resize(triangles.size());  
    float prev = 0;
    for (size_t i = 0; i < triangles.size(); i++) {
      prev += triangles[i].area();
      cdf[i] = prev;
    }
  }


  LightSample MeshLight::sample(V point) const {
    float t = randu() * cdf.back();
    const auto i = std::lower_bound(cdf.begin(), cdf.end(), t) - cdf.begin();
    const float prev_cdf = (i > 0) ? cdf[i - 1] : 0.0;
    const float area = cdf[i] - prev_cdf;
    assert(area > 0);

    const float area_pdf = area / cdf.back();
    assert(area_pdf > 0);

    auto triangle_sample = triangles[i].sample();
    ASSERT_UNIT(triangle_sample.normal);

    const V to = triangle_sample.point - point;
    const float sq_dist = to.dot(to);
    const float dist = sqrt(sq_dist);

    Ray to_light;
    to_light.origin = point;
    to_light.direction = (1.0f / dist) * to;
    ASSERT_UNIT(to_light.direction);

    const float costheta = -triangle_sample.normal.dot(to_light.direction);

    if (costheta <= 0) {
      return LightSample();
    }

    LightSample sample;
    sample.radiance = INVPI * emissive;
    sample.to_light = to_light;
    sample.probability = area_pdf * sq_dist / (area * costheta);
    sample.distance = dist;
    return sample;
  }
}
