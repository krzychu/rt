project(rt)
cmake_minimum_required(VERSION 2.8)

set(CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}")

find_package(OpenEXR REQUIRED)
add_subdirectory(libs/googletest)

add_subdirectory(libs/gflags)

include_directories(
  libs/googletest/include
  ${CMAKE_CURRENT_BINARY_DIR}/libs/gflags/include
  ${OPENEXR_INCLUDE_DIRS}
)

add_definitions("-std=c++11 -Wall -g -O3 -mavx -Wfatal-errors -fno-strict-aliasing -fopenmp -DNDEBUG")

set(CMAKE_EXE_LINKER_FLAGS "-fopenmp")

add_library(
  rt STATIC
  model.cpp
  camera.cpp
  scene.cpp
  math.cpp
  primitives.cpp
  light.cpp
  integrator.cpp
  accelerator.cpp
  moller.cpp
  stats.cpp
  brdf.cpp
)

target_link_libraries(
  rt
  ${OPENEXR_LIBRARIES}
)

add_executable(
  raytracer
  main.cpp
)

target_link_libraries(
  raytracer
  rt
  gflags
)

add_executable(
  tests
  model_test.cpp
  camera_test.cpp
  scene_test.cpp
  moller_test.cpp
  math_test.cpp
  accelerator_test.cpp
  primitives_test.cpp
)

add_custom_command(
  TARGET tests 
  PRE_BUILD 
  COMMAND
    cp ${CMAKE_CURRENT_SOURCE_DIR}/RT.sh ${CMAKE_CURRENT_BINARY_DIR}
)

target_link_libraries(
  tests
  rt
  gtest_main
)

add_executable(
  avx
  avx.cpp
)

target_link_libraries(
  avx
  rt
)
