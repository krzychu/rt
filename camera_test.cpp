// Realistyczna Grafika Komputerowa 2015/2016
// Krzysztof Chrobak

#include <gtest/gtest.h>
#include "camera.h"


using namespace rt;


TEST(CameraTest, rays_have_unit_direction) {
  Camera camera(
      Resolution(10, 20),
      V(1, 1, 1),
      V(2, 1, 1),
      V(1, 2, 1),
      1.0
  );

  auto rays = camera.get_rays();

  ASSERT_EQ(10 * 20, rays.size());
  for (const auto& ray : rays) {
    ASSERT_EQ(ray.origin, V(1, 1, 1)); 
    ASSERT_FLOAT_EQ(1.0, ray.direction.norm());
  }
}


TEST(CameraTest, rays_are_emitted_in_right_direction) {
  Camera camera(
      Resolution(1, 1),
      V(1, 1, 1),
      V(2, 1, 1),
      V(1, 2, 1),
      1.0
  );

  auto ray = camera.get_rays()[0];
  ASSERT_FLOAT_EQ(1, ray.direction.x);
  ASSERT_FLOAT_EQ(0, ray.direction.y);
  ASSERT_FLOAT_EQ(0, ray.direction.z);
}
